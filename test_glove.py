# -*- coding: utf-8 -*-

all_vectors = []

with open('final_vectors.txt', 'r') as f:
    for line in f.readlines():
        try:
            l = line.decode('UTF8').split(u' ')
            l = [l[i] if i == 0 else float(l[i]) for i in xrange(len(l))]
            all_vectors.append(l)
        except Exception as e:
            print e
            print "error", line
            pass

def find(token):
    for vector in all_vectors:
        if token == vector[0]:
            return vector
    return None

def distance2(vec1, vec2):
    d = 0
    for (a,b) in zip(vec1[1:], vec2[1:]):
        d += (b - a)**2
    return d

def find_nearest(vector, maxSize = 10, exactMatch = False):
    result = []
    for v in all_vectors:
        d = distance2(vector, v)
        if d == 0 and not exactMatch:
            continue
        result.append((d, v))
        result = sorted(result)[:-1] if len(result) > maxSize else sorted(result)
    return [v2 for (d2, v2) in result]

def print_nearest(token):
    vec = find(token)
    print token
    if vec != None:
        for name in (x[0] for x in find_nearest(vec)):
            print name
    print "=============="

def print_relation(token1, token2, token3):
    vec1 = find(token1)
    vec2 = find(token2)
    vec3 = find(token3)
    print token1, token2, token3
    if vec1 == None or vec2 == None or vec3 == None:
        print "============="
        return
    new_vec = [u"" if i == 0 else vec2[i]-vec1[i]+vec3[i] for i in xrange(len(vec1))]
    for name in (x[0] for x in find_nearest(new_vec, exactMatch = True)):
        print name
    print "============="

print_nearest(u"اوباما")
print_nearest(u"فرانسه")
print_nearest(u"عراق")
print_nearest(u"استقلال")
print_nearest(u"نفت")
print_nearest(u"دلار")
print_nearest(u"ازدواج")
print_nearest(u"مدرسه")
print_nearest(u"رهبر")
print_nearest(u"پلیس")
print_nearest(u"برنج")
print_nearest(u"امریکا")
print_nearest(u"آمریکا")

print "---------------------"

print_relation(u"آبی", u"قرمز", u"استقلال")
print_relation(u"فرانسه", u"پاریس", u"اسپانیا")
print_relation(u"امریکا", u"اوباما", u"فرانسه")
